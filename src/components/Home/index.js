import React from 'react'
import './home.css'

function Home(props) {
    return (
        <div className="home">
            <button onClick={() => props.history.push('/privilege')}>privileged user</button>
            <button onClick={() => props.history.push('/unprivilege')}>unprivileged user</button>
        </div>
    )
}

export default Home
