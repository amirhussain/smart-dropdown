## Usage of Smart Dropdown component for your scenes.

```
import React from 'react';
import SmartDropdown from 'PATH';

function screen(){
    return(
        <SmartDropdown
            placeholder="string"
            options={array}
            optionsLimit={number}
            searchable={boolean}
            onSelect={callBackHandler}
            addPrivilege={boolean}
            onNewOptionAdd={callBackHandler}
        />
    )
}

export default screen;
```

**Props that can be passed to the component**

1. placeholder { string } :  specifies the display text if there is no option selected.
2. options { array } : array of strings to show the list of options in the dropdown.
3. optionsLimit { number } : number to handle max no. of items listed in the dropdown.
5. searchable { boolean } : permission to search list items in the dropdown.
4. onSelect { function } : callback function to handle selection in the dropdown.
5. addPrivilege { boolean } : permission to add new list item in the dropdown.
6. onNewOptionAdd { function } : callback funnction to handle add list item in the dropdown.