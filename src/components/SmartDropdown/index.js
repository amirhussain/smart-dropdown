import React from 'react'
import { InputGroup, FormControl } from 'react-bootstrap';
import searchIcon from '../../assets/search.png';
import clearIcon from '../../assets/close.png';

import './dropdown.css'

function SmartDropdown(props) {
    const { placeholder, options, optionsLimit, searchable, onSelect, addPrivilege, onNewOptionAdd } = props
    const [toggleDropdown, setToggleDropdown] = React.useState(false)
    const [dropDownOptions, setDropDownOptions] = React.useState([])
    const [searchValue, setSearchValue] = React.useState("")
    const [selected, setSelected] = React.useState("")

    const handleDropDownOptions = () => {
        let data = [...options]
        if (data.length > 0 && optionsLimit >= 0) {
            data.splice(optionsLimit, data.length)
        }
        setDropDownOptions(data)
    }

    React.useEffect(() => {
        handleDropDownOptions()
    }, [options, optionsLimit])

    const searchHandler = (event) => {
        setSearchValue(event.target.value)
        let filteredData = options.filter(option => (option.match(new RegExp(event.target.value, 'gi'))));
        setDropDownOptions(filteredData)
    }

    const handleDropdownSelect = (value) => {
        setSelected(value[0].toUpperCase() + value.slice(1))
        setToggleDropdown(false)
        setSearchValue("")
        handleDropDownOptions()
        onSelect(value[0].toUpperCase() + value.slice(1))
    }

    const showAllOptions = () => {
        setDropDownOptions(options)
    }

    const onNewOption = () => {
        onNewOptionAdd(searchValue[0].toUpperCase() + searchValue.slice(1))
        handleDropdownSelect(searchValue)
    }

    const handleSearchClear = () => {
        setSearchValue("")
        handleDropDownOptions()
    }

    return (
        <div className="dp-section-block">
            <div onClick={() => setToggleDropdown(!toggleDropdown)}>
                <div className="triangle-down"></div>
                {selected ? selected : placeholder}
            </div>
            {
                toggleDropdown &&
                <div>
                    {
                        searchable && options.length > 0 && optionsLimit > 0 &&
                        <InputGroup className='search-inputgroup'>
                            <img src={searchIcon} alt="search" className="seach-icon" />
                            {
                                searchValue && <img src={clearIcon} alt="search" className="clear-icon" onClick={handleSearchClear} />
                            }
                            <FormControl
                                className="search-input"
                                type="text"
                                name="search"
                                value={searchValue}
                                placeholder="Search..."
                                onChange={searchHandler}
                            />
                        </InputGroup>
                    }
                    {
                        dropDownOptions.map((option, index) => (
                            <p className={selected === option ? "active dp-option-value" : "dp-option-value"} key={index} onClick={() => handleDropdownSelect(option)}>{option}</p>
                        ))
                    }
                    {
                        searchValue && dropDownOptions.length <= 0 &&
                        <div className="search-result">
                            <p><strong>"{searchValue}"</strong> not found</p>
                            {
                                addPrivilege &&
                                <button onClick={onNewOption}>Add & Select</button>
                            }
                        </div>
                    }
                    {
                        !searchValue && dropDownOptions.length <= 0 &&
                        <div className="nolocations">
                            <p>No locations found</p>
                        </div>
                    }
                    {
                        !searchValue ? dropDownOptions.length > 0 && options.length !== dropDownOptions.length ?
                            <span onClick={showAllOptions} className="more-btn">more...</span> : "" : ""
                    }
                </div>
            }
        </div>
    )
}

export default SmartDropdown
