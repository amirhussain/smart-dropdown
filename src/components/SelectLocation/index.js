import React from 'react'
import SmartDropdown from '../SmartDropdown'

import '../Home/home.css'

function SelectLocation(props) {
    const [countries, setCountries] = React.useState([])
    const [privilege, setPrivilege] = React.useState(false)
    const [selectedOption, setSelectedOption] = React.useState("")

    const getCountries = async () => {
        await fetch(`${process.env.REACT_APP_MOCK_API_URL}/countries`)
            .then((response) => {
                return response.json();
            }).then((data) => {
                if (data.countries) {
                    setCountries(data.countries)
                }
            })
    }

    const addCountry = async (country) => {
        await fetch(`${process.env.REACT_APP_MOCK_API_URL}/addcountry?name=${country}`)
            .then((response) => {
                return response.json();
            }).then((data) => {
                if (data.status === "Success") {
                    getCountries()
                }
            })
    }

    React.useEffect(() => {
        getCountries()
    }, [])

    React.useEffect(() => {
        if (props.location.pathname === "/privilege") {
            setPrivilege(true)
        } else {
            setPrivilege(false)
        }
    }, [props.location])

    const onSelectHandler = (value) => {
        console.log("it is log on parent component", value);
        setSelectedOption(value)
    }

    const onAddHandler = (value) => {
        addCountry(value)
    }

    return (
        <div>
            <SmartDropdown
                placeholder="Select a location"
                options={countries}
                optionsLimit={5}
                searchable={true}
                onSelect={onSelectHandler}
                addPrivilege={privilege}
                onNewOptionAdd={onAddHandler}
            />
            {
                selectedOption &&
                <div className="location">
                    <label>Selected country :</label>
                    <strong>{selectedOption}</strong>
                </div>
            }
        </div>
    )
}

export default SelectLocation
