import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Home from './components/Home';
import SelectLocation from './components/SelectLocation';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path='/'>
          <Redirect to='home' />
        </Route>
        <Route exact path='/home' component={Home} />
        <Route exact path='/privilege' component={SelectLocation} />
        <Route exact path='/unprivilege' component={SelectLocation} />
      </Switch>
    </Router>
  );
}

export default App;
